package bassis_test;

import bassis.bassis_bean.BeanFactory;
import bassis.bassis_bean.annotation.Autowired;
import bassis.bassis_bean.annotation.Component;
@Component
public class Test {
	@Autowired(clas=UserServiceImpl.class)
	static UserService userService;
	
	public static void main(String[] args) {
		// 启动bassis框架，不加载web部分
		BeanFactory.init("bassis_test");
		userService.find();
	}
}
