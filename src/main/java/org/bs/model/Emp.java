package org.bs.model;

import bassis.bassis_bean.annotation.Autowired;
import bassis.bassis_bean.annotation.Component;

@Component
public class Emp {
	@Autowired
	private String empName;

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}
	
}
