package org.bs.controllers;

import org.apache.log4j.Logger;
import org.bs.model.User;

import bassis.bassis_bean.annotation.Aop;
import bassis.bassis_bean.annotation.Autowired;
import bassis.bassis_tools.json.GsonUtils;
import bassis.bassis_web.annotation.Controller;
import bassis.bassis_web.annotation.RequestMapping;
import bassis.bassis_web.assist.Resource;
import bassis.bassis_web.assist.StewardResource;
import bassis.bassis_web.work.View;

@Controller("/user")
//@Interceptor("/interceptor/privilege")
public class UserController {
	private static Logger logger = Logger.getLogger(UserController.class);
//	@Autowired(verify="NOTNULL")
	private String tel;
	@Autowired
	private User user;
	private static Resource resource=StewardResource.get("/user");

	
	@RequestMapping
	public View loginH() {
		String sendUrl="http://www.baidu.com";
//		logger.info("user.name:"+user.getName());
		View view=new View(sendUrl, false, user);
		return view;
	}
	@RequestMapping
	public View login() {
		String sendUrl="/controllers/EmpController/res.json";
		logger.info("user.name:"+user.getName());
		View view=new View(sendUrl, false, user);
		return view;
	}
	@RequestMapping("/res")
	@Aop("aop.UserAop")
	public String login2() {
		if(null==user){
			user=new User();
			user.setName("李四");
		}
		logger.info("user.name:"+user.getName());
		System.out.println(resource.getServletResource().getPath());
		return GsonUtils.objectToJson(user);
	}
}
